import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrementA, incrementA, selectCountA, incrementTwo, incrementByAmount } from "./counterSliceA";
import { decrementB, incrementB, selectCountB } from "./counterSliceB";
import styles from "./Counter.module.css";

export function Counter() {
  const countA = useSelector(selectCountA);
  const countB = useSelector(selectCountB);
  const dispatch = useDispatch();
  const [incrementAmount, setIncrementAmount] = useState("2");

  return (
    <div>
      <div className={styles.row}>
        <button className={styles.button} aria-label="Increment value A" onClick={() => dispatch(incrementA())}>
          +
        </button>
        <span className={styles.value}>{countA}</span>
        <button className={styles.button} aria-label="Decrement value A" onClick={() => dispatch(decrementA())}>
          -
        </button>
      </div>
      <div className={styles.row}>
        <button className={styles.button} aria-label="Increment value B" onClick={() => dispatch(incrementB())}>
          +
        </button>
        <span className={styles.value}>{countB}</span>
        <button className={styles.button} aria-label="Decrement value B" onClick={() => dispatch(decrementB())}>
          -
        </button>
      </div>

      <div className={styles.row}>
        <button className={styles.button} aria-label="Increment both" onClick={() => dispatch(incrementTwo())}>
          ++
        </button>
      </div>
      <div className={styles.row}>
        <input
          className={styles.textbox}
          aria-label="Set increment amount"
          value={incrementAmount}
          onChange={(e) => setIncrementAmount(e.target.value)}
        />
        <button className={styles.button} onClick={() => dispatch(incrementByAmount(Number(incrementAmount) || 0))}>
          Increment
        </button>
      </div>
    </div>
  );
}
