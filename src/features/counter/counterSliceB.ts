import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { incrementTwo, incrementByAmount } from "./counterSliceA";

interface CounterState {
  value: number;
}

const initialState: CounterState = {
  value: 0,
};

export const counterSliceB = createSlice({
  name: "counterB",
  initialState,
  reducers: {
    incrementB: (state) => {
      state.value += 1;
    },
    decrementB: (state) => {
      state.value -= 1;
    },
  },
  extraReducers: {
    [incrementTwo.type]: (state) => {
      state.value += 2;
    },
    [incrementByAmount.type]: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
});

export const { incrementB, decrementB } = counterSliceB.actions;

export const selectCountB = (state: RootState) => state.counterB.value;

export default counterSliceB.reducer;
