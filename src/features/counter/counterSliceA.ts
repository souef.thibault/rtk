import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface CounterState {
  value: number;
}

const initialState: CounterState = {
  value: 5,
};

export const counterSliceA = createSlice({
  name: "counterA",
  initialState,
  reducers: {
    incrementA: (state) => {
      state.value += 1;
    },
    decrementA: (state) => {
      state.value -= 1;
    },
    incrementTwo: (state) => {
      state.value += 2;
    },
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
});

export const { incrementA, decrementA, incrementByAmount, incrementTwo } = counterSliceA.actions;

export const selectCountA = (state: RootState) => state.counterA.value;

export default counterSliceA.reducer;
