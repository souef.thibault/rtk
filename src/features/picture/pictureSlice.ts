import axios from "axios";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface IPicture {
  albumId?: number;
  id?: number;
  title?: string;
  url?: string;
  thumbnailUrl?: string;
}

interface PictureState {
  isLoading: boolean;
  data: IPicture;
  isError: boolean;
}

const initialState: PictureState = {
  isLoading: false,
  data: {},
  isError: false,
};

export const changePicture = createAsyncThunk("picture/fetchPicture", (id: number) =>
  axios
    .get(`https://jsonplaceholder.typicode.com/photos/${id}`)
    .then((response) => response.data)
    .catch((error) => error)
);

export const picture = createSlice({
  name: "picture",
  initialState,
  reducers: {},
  extraReducers: {
    [changePicture.pending.type]: (state, action) => {
      state.isLoading = true;
      state.data = {};
      state.isError = false;
    },
    [changePicture.fulfilled.type]: (state, action) => {
      state.isLoading = false;
      state.data = action.payload;
      state.isError = false;
    },
    [changePicture.rejected.type]: (state, action) => {
      state.isLoading = false;
      state.data = {};
      state.isError = true;
    },
  },
});

export const datas = (state: RootState) => state.picture;

export default picture.reducer;
