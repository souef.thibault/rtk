import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { changePicture, datas } from "./pictureSlice";

export function Picture() {
  const datasPicture = useSelector(datas);

  console.log({ datasPicture });
  // const URL = useSelector(pictureURL);
  // const isLoading = useSelector(loading);
  // const isError = useSelector(error);
  const dispatch = useDispatch();

  if (datasPicture.isLoading) return <p>Loading</p>;
  if (datasPicture.isError) return <p>ERROR</p>;

  return (
    <div>
      <button onClick={() => dispatch(changePicture(Math.round(Math.random() * 100)))}>Change Picture</button>
      <div>
        <img src={datasPicture.data.url} alt={datasPicture.data.title} />
      </div>
    </div>
  );
}
