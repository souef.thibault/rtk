import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import counterReducerA from "../features/counter/counterSliceA";
import counterReducerB from "../features/counter/counterSliceB";
import picture from "../features/picture/pictureSlice";

export const store = configureStore({
  reducer: {
    counterA: counterReducerA,
    counterB: counterReducerB,
    picture: picture,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
