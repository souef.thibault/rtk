import React from "react";
import { Counter } from "./features/counter/Counter";
import { Picture } from "./features/picture/Picture";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Picture />
        <Counter />
      </header>
    </div>
  );
}

export default App;
