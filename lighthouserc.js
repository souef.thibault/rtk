module.exports = {
  ci: {
    collect: {
      settings: { chromeFlags: "--no-sandbox" },
    },
    upload: {
      target: "filesystem",
      outputDir: ".lighthouseci",
    },
  },
};
